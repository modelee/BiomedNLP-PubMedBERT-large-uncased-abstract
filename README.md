---
language: en
tags:
- exbert
license: mit
widget:
- text: "[MASK] is a tyrosine kinase inhibitor."
---

## MSR BiomedBERT-large (abstracts only)

<div style="border: 2px solid orange; border-radius:10px; padding:0px 10px; width: fit-content;">

* This model was previously named **"PubMedBERT large (abstracts)"**.
* You can either adopt the new model name "microsoft/BiomedNLP-BiomedBERT-large-uncased-abstract" or update your `transformers` library to version 4.22+ if you need to refer to the old name.

</div>

Pretraining large neural language models, such as BERT, has led to impressive gains on many natural language processing (NLP) tasks. However, most pretraining efforts focus on general domain corpora, such as newswire and Web. A prevailing assumption is that even domain-specific pretraining can benefit by starting from general-domain language models. [Recent work](https://arxiv.org/abs/2007.15779) shows that for domains with abundant unlabeled text, such as biomedicine, pretraining language models from scratch results in substantial gains over continual pretraining of general-domain language models. [Followup work](https://arxiv.org/abs/2112.07869) explores larger model sizes and the impact of these on performance on the BLURB benchmark. 

This BiomedBERT is pretrained from scratch using _abstracts_ from [PubMed](https://pubmed.ncbi.nlm.nih.gov/).

## Citation

If you find BiomedBERT useful in your research, please cite the following paper:

```latex
@misc{https://doi.org/10.48550/arxiv.2112.07869,
  doi = {10.48550/ARXIV.2112.07869},
  url = {https://arxiv.org/abs/2112.07869},
  author = {Tinn, Robert and Cheng, Hao and Gu, Yu and Usuyama, Naoto and Liu, Xiaodong and Naumann, Tristan and Gao, Jianfeng and Poon, Hoifung},
  keywords = {Computation and Language (cs.CL), Machine Learning (cs.LG), FOS: Computer and information sciences, FOS: Computer and information sciences},
  title = {Fine-Tuning Large Neural Language Models for Biomedical Natural Language Processing},
  publisher = {arXiv},
  year = {2021},
  copyright = {arXiv.org perpetual, non-exclusive license}
}
```

<a href="https://huggingface.co/exbert/?model=microsoft/BiomedNLP-PubMedBERT-large-uncased-abstract&modelKind=bidirectional&sentence=Gefitinib%20is%20an%20EGFR%20tyrosine%20kinase%20inhibitor,%20which%20is%20often%20used%20for%20breast%20cancer%20and%20NSCLC%20treatment.&layer=10&heads=..0,1,2,3,4,5,6,7,8,9,10,11&threshold=0.7&tokenInd=17&tokenSide=right&maskInds=..&hideClsSep=true">
	<img width="300px" src="https://cdn-media.huggingface.co/exbert/button.png">
</a>
